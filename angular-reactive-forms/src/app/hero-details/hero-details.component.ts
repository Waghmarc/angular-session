import { Component, OnInit,Input } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms'
import { states, Hero } from '../data-model';

@Component({
  selector: 'app-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.css']
})
export class HeroDetailsComponent implements OnInit {
  // name = new FormControl();
  //  heroForm = new FormGroup (
  //    { name: new FormControl() 
  // }); 
//   heroForm: FormGroup; // <--- heroForm is of type FormGroup 
//   constructor(private fb: FormBuilder) // <--- inject FormBuilder 
//   { 
//     this.createForm(); 
//   } 
//   createForm() 
//   { this.heroForm = this.fb.group(
//     {
//        name:['', Validators.required ],    // <--- the FormControl called "name"
//   }); 
//  }
@Input() hero: Hero;

 heroForm: FormGroup; states = states;
  constructor(private fb: FormBuilder) 
  { this.createForm(); } 
  createForm() { 
    this.heroForm = this.fb.group({ 
      name: ['', Validators.required ],
       street: '',
        city: '', 
        state: '', 
        zip: '', 
        power: '', 
        sidekick: '' 
      });
       }
  ngOnInit() {
  }

}
