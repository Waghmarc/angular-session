import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeroDetailsComponent } from './hero-details/hero-details.component';
import { HeroService } from './hero.service';
import { HeroListComponent } from './hero-list/hero-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroDetailsComponent,
    HeroListComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule 
  ],
  exports: [ AppComponent,
      HeroDetailsComponent,
      HeroListComponent ],// <-- export HeroListComponent 
  providers: [HeroService],
  bootstrap: [AppComponent]
})
export class AppModule { }
